var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");
var app = express();

app.use(logger("dev"));
app.use(bodyParser.json());
app.use("/", require("./companies"));
app.use(function (req, res, next) {
	var err = new Error("Not Found");
	err.status = 404;
	next(err);
});

module.exports = app.listen(7575);