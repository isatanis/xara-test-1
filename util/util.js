var util = {
	wrappedError: function (statusCode, error) {
		//wrap the error for more readibility.
		return {
			statusCode: statusCode,
			error: error
		};
	}
};
module.exports = util;