var MongoClient = require("mongodb").MongoClient;
var url = "mongodb://localhost:27017/Business";

function DBContext() {}
DBContext.prototype.open = function (callback) {
	// Connection URL
	// Use connect method to connect to the server
	MongoClient.connect(url, function (err, db) {
		return callback(err, db);
	});
};
module.exports = DBContext;