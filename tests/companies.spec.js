process.env.NODE_ENV = "test";

//Require the dev-dependencies
var assert = require("assert");
var companies = require("../companies/companies");
var validationRules = require("../companies/validation.rules");

var createdCompanyIds = [];
var createdWorkspaceIds = [];
describe("Business.Company", function () {
	describe("Company", function () {
		it("it should create a first company succesfully", function (done) {
			var requestObject = {
				displayName: "Xara"
			};
			companies.createCompany(requestObject, function (err, createdCompany) {
				assert.ifError(err);
				assert.ok(createdCompany._id);
				createdCompanyIds.push(createdCompany._id);
				assert.equal(requestObject.displayName, createdCompany.displayName);
				assert.equal(requestObject.displayName.toLowerCase(), createdCompany.name);
				assert.deepEqual([], createdCompany.workspaces);
				done();
			});
		});
		it("it should create a second company succesfully", function (done) {
			var requestObject = {
				displayName: "Google"
			};
			companies.createCompany(requestObject, function (err, createdCompany) {
				assert.ifError(err);
				assert.ok(createdCompany._id);
				createdCompanyIds.push(createdCompany._id);
				assert.equal(requestObject.displayName, createdCompany.displayName);
				assert.equal(requestObject.displayName.toLowerCase(), createdCompany.name);
				assert.deepEqual([], createdCompany.workspaces);
				done();
			});
		});
		it("it should not create a duplicate company", function (done) {
			var requestObject = {
				displayName: "Xara"
			};
			companies.createCompany(requestObject, function (err, createdCompany) {
				assert.ifError(createdCompany);
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal(11000, err.error.code);
				done();
			});
		});
		it("it should not create a company with empty display name", function (done) {
			var requestObject = {
				displayName: ""
			};
			companies.createCompany(requestObject, function (err, createdCompany) {
				assert.ifError(createdCompany);
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal(1, err.error.displayName.minLength);
				done();
			});
		});
		it("it should not create a company without display name", function (done) {
			var requestObject = {
				displayName: null
			};
			companies.createCompany(requestObject, function (err, createdCompany) {
				assert.ifError(createdCompany);
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal(1, err.error.displayName.minLength);
				done();
			});
		});
		it("it should update a company", function (done) {
			var requestObject = {
				displayName: "XaraUpdated"
			};
			companies.updateCompany(createdCompanyIds[0], requestObject, function (err) {
				assert.ifError(err);
				done();
			});
		});
		it("it should update a company with string company id", function (done) {
			var requestObject = {
				displayName: "XaraUpdated2"
			};
			companies.updateCompany(createdCompanyIds[0].toHexString(), requestObject, function (err) {
				assert.ifError(err);
				done();
			});
		});
		it("it should not update a company without company id", function (done) {
			var requestObject = {
				displayName: "XaraUpdated"
			};
			companies.updateCompany(null, requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal("Invalid company id", err.error);
				done();
			});
		});
		it("it should not update a company with empty company id", function (done) {
			var requestObject = {
				displayName: "XaraUpdated"
			};
			companies.updateCompany("", requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal("Invalid company id", err.error);
				done();
			});
		});
		it("it should not update a company with empty display name", function (done) {
			var requestObject = {
				displayName: ""
			};
			companies.updateCompany(createdCompanyIds[0], requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.deepEqual(validationRules.company, err.error);
				done();
			});
		});
	});
	describe("Workspace", function () {
		it("it should create first workspace to first company succesfully", function (done) {
			var requestObject = {
				displayName: "Xara Berlin"
			};
			companies.createWorkspace(createdCompanyIds[0], requestObject, function (err, createdWorkspace) {
				assert.ifError(err);
				assert.ok(createdWorkspace._id);

				createdWorkspaceIds.push(createdWorkspace._id);
				assert.equal(requestObject.displayName, createdWorkspace.displayName);
				assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
				assert.deepEqual([], createdWorkspace.users);
				done();
			});
		});
		it("it should create second workspace to first company succesfully", function (done) {
			var requestObject = {
				displayName: "Xara London"
			};
			companies.createWorkspace(createdCompanyIds[0], requestObject, function (err, createdWorkspace) {
				assert.ifError(err);
				assert.ok(createdWorkspace._id);

				createdWorkspaceIds.push(createdWorkspace._id);
				assert.equal(requestObject.displayName, createdWorkspace.displayName);
				assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
				assert.deepEqual([], createdWorkspace.users);
				done();
			});
		});
		it("it should create first workspace to second company succesfully", function (done) {
			var requestObject = {
				displayName: "Google Berlin"
			};
			companies.createWorkspace(createdCompanyIds[1], requestObject, function (err, createdWorkspace) {
				assert.ifError(err);
				assert.ok(createdWorkspace._id);

				createdWorkspaceIds.push(createdWorkspace._id);
				assert.equal(requestObject.displayName, createdWorkspace.displayName);
				assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
				assert.deepEqual([], createdWorkspace.users);
				done();
			});
		});
		it("it should create second workspace to second company succesfully", function (done) {
			var requestObject = {
				displayName: "Google London"
			};
			companies.createWorkspace(createdCompanyIds[1], requestObject, function (err, createdWorkspace) {
				assert.ifError(err);
				assert.ok(createdWorkspace._id);

				createdWorkspaceIds.push(createdWorkspace._id);
				assert.equal(requestObject.displayName, createdWorkspace.displayName);
				assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
				assert.deepEqual([], createdWorkspace.users);
				done();
			});
		});
		it("it should create second workspace of second company to first company succesfully", function (done) {
			var requestObject = {
				displayName: "Google London"
			};
			companies.createWorkspace(createdCompanyIds[0], requestObject, function (err, createdWorkspace) {
				assert.ifError(err);
				assert.ok(createdWorkspace._id);

				createdWorkspaceIds.push(createdWorkspace._id);
				assert.equal(requestObject.displayName, createdWorkspace.displayName);
				assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
				assert.deepEqual([], createdWorkspace.users);
				done();
			});
		});
		it("it should create first workspace of first company to second company succesfully", function (done) {
			var requestObject = {
				displayName: "Xara Berlin"
			};
			companies.createWorkspace(createdCompanyIds[1], requestObject, function (err, createdWorkspace) {
				assert.ifError(err);
				assert.ok(createdWorkspace._id);

				createdWorkspaceIds.push(createdWorkspace._id);
				assert.equal(requestObject.displayName, createdWorkspace.displayName);
				assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
				assert.deepEqual([], createdWorkspace.users);
				done();
			});
		});
		it("it should not create a duplicate workspace to first company", function (done) {
			var requestObject = {
				displayName: "Xara Berlin"
			};
			companies.createWorkspace(createdCompanyIds[0], requestObject, function (err, createdWorkspace) {

				assert.ifError(createdWorkspace);
				assert.ok(err);

				assert.equal(404, err.statusCode);
				assert.equal("The company not found or the workspace is already exist.", err.error);
				done();
			});
		});
		it("it should not create a workspace to first company with empty display name", function (done) {
			var requestObject = {
				displayName: ""
			};
			companies.createWorkspace(createdCompanyIds[0], requestObject, function (err, createWorkspace) {
				assert.ifError(createWorkspace);
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal(1, err.error.displayName.minLength);
				done();
			});
		});
		it("it should not create a workspace to first company without display name", function (done) {
			var requestObject = {
				displayName: null
			};
			companies.createWorkspace(createdCompanyIds[0], requestObject, function (err, createWorkspace) {
				assert.ifError(createWorkspace);
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal(1, err.error.displayName.minLength);
				done();
			});
		});
		it("it should update first workspace from first company", function (done) {
			var requestObject = {
				displayName: "Xara Berlin Updated"
			};
			companies.updateWorkspace(createdCompanyIds[0], createdWorkspaceIds[0], requestObject, function (err) {

				assert.ifError(err);
				done();
			});
		});
		it("it should update second workspace from second company", function (done) {
			var requestObject = {
				displayName: "Google London Updated"
			};
			companies.updateWorkspace(createdCompanyIds[1], createdWorkspaceIds[3], requestObject, function (err) {
				assert.ifError(err);
				done();
			});
		});
		it("it should not update first workspace from first company without company id", function (done) {
			var requestObject = {
				displayName: "XaraUpdated"
			};
			companies.updateWorkspace(null, createdWorkspaceIds[0], requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal("Invalid company id", err.error);
				done();
			});
		});
		it("it should not update first workspace from first company with empty workspace id", function (done) {
			var requestObject = {
				displayName: "XaraUpdated"
			};
			companies.updateWorkspace(createdCompanyIds[0], "", requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal("Invalid workspace id", err.error);
				done();
			});
		});
		it("it should not update first workspace from first company without workspace id", function (done) {
			var requestObject = {
				displayName: "XaraUpdated"
			};
			companies.updateWorkspace(createdCompanyIds[0], null, requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal("Invalid workspace id", err.error);
				done();
			});
		});
		it("it should not update second workspace from second company with empty display name", function (done) {
			var requestObject = {
				displayName: ""
			};
			companies.updateWorkspace(createdCompanyIds[1], createdWorkspaceIds[3], requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.deepEqual(validationRules.company, err.error);
				done();
			});
		});
	});
	describe("Users", function () {
		it("it should add basic user to first workspace of first company succesfully", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			companies.addUser(createdCompanyIds[0], createdWorkspaceIds[0], requestObject, function (err) {
				assert.ifError(err);
				done();
			});
		});
		it("it should add admin user to first workspace of first company succesfully", function (done) {
			var requestObject = {
				email: "robertot@xara.com",
				role: "admin"
			};
			companies.addUser(createdCompanyIds[0], createdWorkspaceIds[0], requestObject, function (err) {
				assert.ifError(err);
				done();
			});
		});
		it("it should add basic user to second workspace of second company succesfully", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			companies.addUser(createdCompanyIds[1], createdWorkspaceIds[3], requestObject, function (err) {
				assert.ifError(err);
				done();
			});
		});
		it("it should add admin user to first workspace of second company succesfully", function (done) {
			var requestObject = {
				email: "robertot@xara.com",
				role: "admin"
			};
			companies.addUser(createdCompanyIds[1], createdWorkspaceIds[2], requestObject, function (err) {
				assert.ifError(err);
				done();
			});
		});
		it("it should not add duplicate admin user to first workspace of second company ", function (done) {
			var requestObject = {
				email: "robertot@xara.com",
				role: "admin"
			};
			companies.addUser(createdCompanyIds[1], createdWorkspaceIds[2], requestObject, function (err) {
				assert.ok(err);
				assert.equal(404, err.statusCode);
				assert.equal("The workspace not found or the user is already exist.", err.error);
				done();
			});
		});
		it("it should not add a user to first workspace of second company with unknown role", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "manager"
			};
			companies.addUser(createdCompanyIds[1], createdWorkspaceIds[2], requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.deepEqual(["basic", "admin"], err.error.role.enum);
				done();
			});
		});
		it("it should not add a user to first workspace of second company with invalid email", function (done) {
			var requestObject = {
				email: "xarahotmail.com",
				role: "basic"
			};
			companies.addUser(createdCompanyIds[1], createdWorkspaceIds[2], requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				// eslint-disable-next-line no-useless-escape
				assert.deepEqual("^.+\@.+$", err.error.email.pattern);
				done();
			});
		});
		it("it should remove basic user to first workspace of first company succesfully", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			companies.removeUser(createdCompanyIds[0], createdWorkspaceIds[0], requestObject, function (err) {
				assert.ifError(err);
				done();
			});
		});
		it("it should remove admin user to first workspace of second company succesfully", function (done) {
			var requestObject = {
				email: "robertot@xara.com",
				role: "admin"
			};
			companies.removeUser(createdCompanyIds[1], createdWorkspaceIds[2], requestObject, function (err) {
				assert.ifError(err);
				done();
			});
		});
		it("it should not remove unknown admin user to first workspace of second company", function (done) {
			var requestObject = {
				email: "albert@xara.com",
				role: "admin"
			};
			companies.removeUser(createdCompanyIds[1], createdWorkspaceIds[2], requestObject, function (err) {
				assert.ok(err);
				assert.equal(500, err.statusCode);
				done();
			});
		});
		it("it should not remove a user to first workspace of second company without company id", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			companies.removeUser(null, createdWorkspaceIds[2], requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal("Invalid company id", err.error);
				done();
			});
		});
		it("it should not remove a user to first workspace of second company without workspace id", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			companies.removeUser(createdCompanyIds[1], null, requestObject, function (err) {
				assert.ok(err);
				assert.equal(400, err.statusCode);
				assert.equal("Invalid workspace id", err.error);
				done();
			});
		});
	});
	after("Removing test companies", function (done) {
		companies.removeCompany(createdCompanyIds, function (err, removeResult) {
			assert.ifError(err);
			assert.equal(1, removeResult.ok);
			assert.equal(createdCompanyIds.length, removeResult.n);
			done();
		});
	});
});