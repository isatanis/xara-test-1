process.env.NODE_ENV = "test";
var validationRules = require("../companies/validation.rules");
var assert = require("assert");
describe("Validation Rules", function () {
	it("it should correct the company validation rule", function (done) {
		assert.ok(validationRules.company);
		assert.equal(1, validationRules.company.displayName.minLength);
		assert.equal("string", validationRules.company.displayName.type);
		assert.equal("must be a string, required and unique", validationRules.company.displayName.description);
		done();
	});
	it("it should correct the workspace validation rule", function (done) {
		assert.ok(validationRules.workspace);
		assert.equal(1, validationRules.workspace.displayName.minLength);
		assert.equal("string", validationRules.workspace.displayName.type);
		assert.equal("must be a string, required and unique", validationRules.workspace.displayName.description);
		done();
	});
	it("it should correct the user validation rule", function (done) {
		assert.ok(validationRules.user);
		assert.deepEqual(["basic", "admin"], validationRules.user.role.enum);
		assert.equal("can only be one of the enum values", validationRules.user.role.description);
		assert.equal("string", validationRules.user.email.type);
		// eslint-disable-next-line no-useless-escape
		assert.equal("^.+\@.+$", validationRules.user.email.pattern);
		assert.equal("must be a valid email address and unique", validationRules.user.email.description);
		done();
	});
});