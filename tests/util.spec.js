process.env.NODE_ENV = "test";
var util = require("../util/util");
var assert = require("assert");
describe("Utils", function () {
	it("it should correct the util.wrappedError", function (done) {
		assert.deepEqual({
			statusCode: 200,
			error: null
		}, util.wrappedError(200, null));
		done();
	});
	it("it should not correct the util.wrappedError", function (done) {
		assert.notDeepEqual({
			statusCode: 200,
			error: "Invalid workspace id"
		}, util.wrappedError(400, "Invalid company id"));
		done();
	});
});