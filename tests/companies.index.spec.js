process.env.NODE_ENV = "test";

//Require the dev-dependencies
var assert = require("assert");
var validationRules = require("../companies/validation.rules");
var request = require("supertest");
var companies = require("../companies/companies");
var service = require("../service");
var createdCompanyIds = [];
var createdWorkspaceIds = [];
var ObjectId = require("mongodb").ObjectId;

var server = request(service);
describe("Business", function () {
	describe("Company", function () {
		it("it should create a first company succesfully", function (done) {
			var requestObject = {
				displayName: "Xara"
			};
			server
				.post("/company/create")
				.send(requestObject)
				.expect(200)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var createdCompany = res.body;
					assert.ok(createdCompany._id);
					createdCompanyIds.push(createdCompany._id);
					assert.equal(requestObject.displayName, createdCompany.displayName);
					assert.equal(requestObject.displayName.toLowerCase(), createdCompany.name);
					assert.deepEqual([], createdCompany.workspaces);
					done();
				});
		});
		it("it should create a second company succesfully", function (done) {
			var requestObject = {
				displayName: "Google"
			};
			server
				.post("/company/create")
				.send(requestObject)
				.expect(200)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var createdCompany = res.body;
					assert.ok(createdCompany._id);
					createdCompanyIds.push(createdCompany._id);
					assert.equal(requestObject.displayName, createdCompany.displayName);
					assert.equal(requestObject.displayName.toLowerCase(), createdCompany.name);
					assert.deepEqual([], createdCompany.workspaces);
					done();
				});
		});
		it("it should not create a duplicate company", function (done) {
			var requestObject = {
				displayName: "Xara"
			};
			server
				.post("/company/create")
				.send(requestObject)
				.expect(400)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(400, resultError.statusCode);
					assert.equal(11000, resultError.error.code);
					done();
				});
		});
		it("it should not create a company with empty display name", function (done) {
			var requestObject = {
				displayName: ""
			};
			server
				.post("/company/create")
				.send(requestObject)
				.expect(400)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(400, resultError.statusCode);
					assert.equal(1, resultError.error.displayName.minLength);
					done();
				});
		});
		it("it should not create a company without display name", function (done) {
			var requestObject = {
				displayName: null
			};
			server
				.post("/company/create")
				.send(requestObject)
				.expect(400)
				.end(function (err, res) {
					//if (err) return done(err);
					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(400, resultError.statusCode);
					assert.equal(1, resultError.error.displayName.minLength);
					done();
				});
		});
		it("it should update a company", function (done) {
			var requestObject = {
				displayName: "XaraUpdated"
			};
			var path = "/company/update/" + createdCompanyIds[0];
			server
				.patch(path)
				.send(requestObject)
				.expect(204)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should not update a company with empty company id", function (done) {
			var requestObject = {
				displayName: "XaraUpdated"
			};
			var path = "/company/update/" + "";
			server
				.patch(path)
				.send(requestObject)
				.expect(404)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should not update a company with empty display name", function (done) {
			var requestObject = {
				displayName: ""
			};
			var path = "/company/update/" + createdCompanyIds[0];
			server
				.patch(path)
				.send(requestObject)
				.expect(400)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(400, resultError.statusCode);
					assert.deepEqual(validationRules.company, resultError.error);
					done();
				});
		});
	});
	describe("Workspace", function () {
		it("it should create first workspace to first company succesfully", function (done) {
			var requestObject = {
				displayName: "Xara Berlin"
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/create";
			server
				.post(path)
				.send(requestObject)
				.expect(200)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var createdWorkspace = res.body;
					assert.ok(createdWorkspace._id);
					createdWorkspaceIds.push(createdWorkspace._id);
					assert.equal(requestObject.displayName, createdWorkspace.displayName);
					assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
					assert.deepEqual([], createdWorkspace.users);
					done();
				});
		});
		it("it should create second workspace to first company succesfully", function (done) {
			var requestObject = {
				displayName: "Xara London"
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/create";
			server
				.post(path)
				.send(requestObject)
				.expect(200)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var createdWorkspace = res.body;
					assert.ok(createdWorkspace._id);
					createdWorkspaceIds.push(createdWorkspace._id);
					assert.equal(requestObject.displayName, createdWorkspace.displayName);
					assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
					assert.deepEqual([], createdWorkspace.users);
					done();
				});
		});
		it("it should create first workspace to second company succesfully", function (done) {
			var requestObject = {
				displayName: "Google Berlin"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/create";
			server
				.post(path)
				.send(requestObject)
				.expect(200)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var createdWorkspace = res.body;
					assert.ok(createdWorkspace._id);
					createdWorkspaceIds.push(createdWorkspace._id);
					assert.equal(requestObject.displayName, createdWorkspace.displayName);
					assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
					assert.deepEqual([], createdWorkspace.users);
					done();
				});
		});
		it("it should create second workspace to second company succesfully", function (done) {
			var requestObject = {
				displayName: "Google London"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/create";
			server
				.post(path)
				.send(requestObject)
				.expect(200)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var createdWorkspace = res.body;
					assert.ok(createdWorkspace._id);
					createdWorkspaceIds.push(createdWorkspace._id);
					assert.equal(requestObject.displayName, createdWorkspace.displayName);
					assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
					assert.deepEqual([], createdWorkspace.users);
					done();
				});
		});
		it("it should create second workspace of second company to first company succesfully", function (done) {
			var requestObject = {
				displayName: "Google London"
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/create";
			server
				.post(path)
				.send(requestObject)
				.expect(200)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var createdWorkspace = res.body;
					assert.ok(createdWorkspace._id);
					createdWorkspaceIds.push(createdWorkspace._id);
					assert.equal(requestObject.displayName, createdWorkspace.displayName);
					assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
					assert.deepEqual([], createdWorkspace.users);
					done();
				});
		});
		it("it should create first workspace of first company to second company succesfully", function (done) {
			var requestObject = {
				displayName: "Xara Berlin"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/create";
			server
				.post(path)
				.send(requestObject)
				.expect(200)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var createdWorkspace = res.body;
					assert.ok(createdWorkspace._id);
					createdWorkspaceIds.push(createdWorkspace._id);
					assert.equal(requestObject.displayName, createdWorkspace.displayName);
					assert.equal(requestObject.displayName.toLowerCase(), createdWorkspace.name);
					assert.deepEqual([], createdWorkspace.users);
					done();
				});
		});
		it("it should not create a duplicate workspace to first company", function (done) {
			var requestObject = {
				displayName: "Xara Berlin"
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/create";
			server
				.post(path)
				.send(requestObject)
				.expect(404)
				.end(function (err, res) {
					//if (err) return done(err);
					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(404, resultError.statusCode);
					assert.equal("The company not found or the workspace is already exist.", resultError.error);
					done();
				});
		});
		it("it should not create a workspace to first company with empty display name", function (done) {
			var requestObject = {
				displayName: ""
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/create";
			server
				.post(path)
				.send(requestObject)
				.expect(400)
				.end(function (err, res) {
					//if (err) return done(err);
					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(400, resultError.statusCode);
					assert.equal(1, resultError.error.displayName.minLength);
					done();
				});
		});
		it("it should not create a workspace to first company without display name", function (done) {
			var requestObject = {
				displayName: null
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/create";
			server
				.post(path)
				.send(requestObject)
				.expect(400)
				.end(function (err, res) {
					if (err) return done(err);
					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(400, resultError.statusCode);
					assert.equal(1, resultError.error.displayName.minLength);
					done();
				});
		});
		it("it should update first workspace from first company", function (done) {
			var requestObject = {
				displayName: "Xara Berlin Updated"
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/update/" + createdWorkspaceIds[0];
			server
				.patch(path)
				.send(requestObject)
				.expect(204)
				.end(function (err, res) {
					if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should update second workspace from second company", function (done) {
			var requestObject = {
				displayName: "Google London Updated"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/update/" + createdWorkspaceIds[3];
			server
				.patch(path)
				.send(requestObject)
				.expect(204)
				.end(function (err, res) {
					if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should not update first workspace from first company with empty workspace id", function (done) {
			var requestObject = {
				displayName: "XaraUpdated"
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/update/";
			server
				.patch(path)
				.send(requestObject)
				.expect(404)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should not update second workspace from second company with empty display name", function (done) {
			var requestObject = {
				displayName: ""
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/update/" + createdWorkspaceIds[3];
			server
				.patch(path)
				.send(requestObject)
				.expect(400)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(400, resultError.statusCode);
					assert.deepEqual(validationRules.company, resultError.error);
					done();
				});
		});
	});
	describe("Users", function () {
		it("it should add basic user to first workspace of first company succesfully", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/" + createdWorkspaceIds[0] + "/user/add";
			server
				.post(path)
				.send(requestObject)
				.expect(201)
				.end(function (err, res) {
					if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should add admin user to first workspace of first company succesfully", function (done) {
			var requestObject = {
				email: "robertot@xara.com",
				role: "admin"
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/" + createdWorkspaceIds[0] + "/user/add";
			server
				.post(path)
				.send(requestObject)
				.expect(201)
				.end(function (err, res) {
					if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should add basic user to second workspace of second company succesfully", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/" + createdWorkspaceIds[3] + "/user/add";
			server
				.post(path)
				.send(requestObject)
				.expect(201)
				.end(function (err, res) {
					if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should add admin user to first workspace of second company succesfully", function (done) {
			var requestObject = {
				email: "robertot@xara.com",
				role: "admin"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/" + createdWorkspaceIds[2] + "/user/add";
			server
				.post(path)
				.send(requestObject)
				.expect(201)
				.end(function (err, res) {
					if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should not add duplicate admin user to first workspace of second company ", function (done) {
			var requestObject = {
				email: "robertot@xara.com",
				role: "admin"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/" + createdWorkspaceIds[2] + "/user/add";
			server
				.post(path)
				.send(requestObject)
				.expect(404)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(404, resultError.statusCode);
					assert.equal("The workspace not found or the user is already exist.", resultError.error);
					done();
				});
		});
		it("it should not add a user to first workspace of second company with unknown role", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "manager"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/" + createdWorkspaceIds[2] + "/user/add";
			server
				.post(path)
				.send(requestObject)
				.expect(400)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(400, resultError.statusCode);
					assert.deepEqual(["basic", "admin"], resultError.error.role.enum);
					done();
				});
		});
		it("it should not add a user to first workspace of second company with invalid email", function (done) {
			var requestObject = {
				email: "xarahotmail.com",
				role: "basic"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/" + createdWorkspaceIds[2] + "/user/add";
			server
				.post(path)
				.send(requestObject)
				.expect(400)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					var resultError = res.body;
					assert.equal(400, resultError.statusCode);
					// eslint-disable-next-line no-useless-escape
					assert.deepEqual("^.+\@.+$", resultError.error.email.pattern);
					done();
				});
		});
		it("it should remove basic user to first workspace of first company succesfully", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			var path = "/company/" + createdCompanyIds[0] + "/workspace/" + createdWorkspaceIds[0] + "/user/remove";
			server
				.delete(path)
				.send(requestObject)
				.expect(204)
				.end(function (err, res) {
					if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should remove admin user to first workspace of second company succesfully", function (done) {
			var requestObject = {
				email: "robertot@xara.com",
				role: "admin"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/" + createdWorkspaceIds[2] + "/user/remove";
			server
				.delete(path)
				.send(requestObject)
				.expect(204)
				.end(function (err, res) {
					if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should not remove unknown admin user to first workspace of second company", function (done) {
			var requestObject = {
				email: "albert@xara.com",
				role: "admin"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/" + createdWorkspaceIds[2] + "/user/remove";
			server
				.delete(path)
				.send(requestObject)
				.expect(500)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					assert.equal(500, res.body.statusCode);
					done();
				});
		});
		it("it should not remove a user to first workspace of second company without company id", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			var path = "/company/" + "" + "/workspace/" + createdWorkspaceIds[2] + "/user/remove";
			server
				.delete(path)
				.send(requestObject)
				.expect(404)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
		it("it should not remove a user to first workspace of second company without workspace id", function (done) {
			var requestObject = {
				email: "isatns@hotmail.com",
				role: "basic"
			};
			var path = "/company/" + createdCompanyIds[1] + "/workspace/" + "" + "/user/remove";
			server
				.delete(path)
				.send(requestObject)
				.expect(404)
				.end(function (err, res) {
					//if (err) return done(err);

					assert.ok(res.body);
					done();
				});
		});
	});
	after("Removing test companies", function (done) {
		var createdCompanyObjectIds = [];
		createdCompanyIds.forEach(function (value) {
			createdCompanyObjectIds.push(new ObjectId(value));
		});
		companies.removeCompany(createdCompanyObjectIds, function (err, removeResult) {
			assert.ifError(err);
			assert.equal(1, removeResult.ok);
			assert.equal(createdCompanyIds.length, removeResult.n);
			done();
		});
	});
});