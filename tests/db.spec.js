process.env.NODE_ENV = "test";
var DBConnector = require("../util/db");
var dbContext = new DBConnector();
var assert = require("assert");
describe("DB Connection", function () {
	it("it should connect db", function (done) {
		dbContext.open(function (err, dbConnection) {
			assert.ifError(err);
			assert.ok(dbConnection);
			var dbCollection = dbConnection.collection("companies");
			assert.equal("companies", dbCollection.collectionName);
			dbConnection.close();
			done();
		});
	});
});