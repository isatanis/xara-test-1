var express = require("express");
var router = express.Router();
var companies = require("./companies.js");

router.post("/company/create", function (req, res) {
	var payload = req.body;
	companies.createCompany(payload, function (err, createdCompany) {
		if (err) {
			res.status(err.statusCode).json(err);
		} else {
			res.status(200).json(createdCompany);
		}
	});
});
router.patch("/company/update/:companyId", function (req, res) {
	var companyId = req.params.companyId;
	console.error(companyId);
	var payload = req.body;
	companies.updateCompany(companyId, payload, function (err) {
		if (err) {
			res.status(err.statusCode).json(err);
		} else {
			res.sendStatus(204);
		}
	});
});
router.post("/company/:companyId/workspace/create", function (req, res) {
	var companyId = req.params.companyId;
	var payload = req.body;
	companies.createWorkspace(companyId, payload, function (err, createdWorkspace) {
		if (err) {
			res.status(err.statusCode).json(err);
		} else {
			res.status(200).json(createdWorkspace);
		}
	});
});
router.patch("/company/:companyId/workspace/update/:workspaceId", function (req, res) {
	var companyId = req.params.companyId;
	var workspaceId = req.params.workspaceId;
	var payload = req.body;
	companies.updateWorkspace(companyId, workspaceId, payload, function (err) {
		if (err) {
			res.status(err.statusCode).json(err);
		} else {
			res.sendStatus(204);
		}
	});
});
router.post("/company/:companyId/workspace/:workspaceId/user/add", function (req, res) {
	var companyId = req.params.companyId;
	var workspaceId = req.params.workspaceId;
	var payload = req.body;
	companies.addUser(companyId, workspaceId, payload, function (err) {
		if (err) {
			res.status(err.statusCode).json(err);
		} else {
			res.sendStatus(201);
		}
	});
});
router.delete("/company/:companyId/workspace/:workspaceId/user/remove", function (req, res) {
	var companyId = req.params.companyId;
	var workspaceId = req.params.workspaceId;
	var payload = req.body;
	companies.removeUser(companyId, workspaceId, payload, function (err) {
		if (err) {
			res.status(err.statusCode).json(err);
		} else {
			res.sendStatus(204);
		}
	});
});

module.exports = router;