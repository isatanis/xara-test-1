var DBConnector = require("../util/db");
var dbContext = new DBConnector();
var ObjectId = require("mongodb").ObjectId;
var ValidationRules = require("./validation.rules");
var Util = require("../util/util");
var VALIDATION_ERROR_CODE = 121;
var companiesService = {
	createCompany: function (payload, callback) {
		//Check if display name is exist
		if (!payload || !payload.displayName) {
			return callback(Util.wrappedError(400, ValidationRules.company), null);
		}
		//Open DB connection
		dbContext.open(function (dbError, dbConnection) {
			if (dbConnection) {
				//generate company object
				var companyObject = {
					displayName: payload.displayName,
					name: payload.displayName.toLowerCase(),
					workspaces: []
				};
				//select companies collection
				var companiesCollection = dbConnection.collection("companies");
				companiesCollection.insertOne(companyObject, function (err, insertRes) {
					dbConnection.close();
					if (err) {
						//If the error is a validation error, return the error of validation, if not, return the error.
						return callback(Util.wrappedError(400, (err.code == VALIDATION_ERROR_CODE) ? ValidationRules.company : err), null);
					} else if (insertRes.insertedCount == 0) {
						return callback(Util.wrappedError(500, "The company could not be created."), null);
					} else {
						return callback(null, insertRes.ops[0]);
					}
				});
			} else {
				//Could not connect to database.
				return callback(Util.wrappedError(500, dbError), null);
			}
		});
	},
	updateCompany: function (companyId, payload, callback) {
		//Check if the company id is a valid objectid
		if (!ObjectId.isValid(companyId)) {
			return callback(Util.wrappedError(400, "Invalid company id"));

		}
		//Check if display name is exist
		if (!payload || !payload.displayName) {
			return callback(Util.wrappedError(400, ValidationRules.company));

		}
		//Open DB connection
		dbContext.open(function (dbError, dbConnection) {
			if (dbConnection) {
				//generate company object for update
				var companyUpdateObject = {
					displayName: payload.displayName,
					name: payload.displayName.toLowerCase()
				};
				//select companies collection
				var companiesCollection = dbConnection.collection("companies");
				companiesCollection.updateOne({
					_id: new ObjectId(companyId)
				}, {
					$set: companyUpdateObject
				}, function (err, updateRes) {

					dbConnection.close();
					if (err) {
						//If the error is a validation error, return the error of validation, if not, return the error.
						return callback(Util.wrappedError(400, (err.code == VALIDATION_ERROR_CODE) ? ValidationRules.company : err));
					} else if (updateRes.matchedCount == 0) {
						return callback(Util.wrappedError(404, "The company not found"));
					} else if (updateRes.modifiedCount == 0) {
						return callback(Util.wrappedError(500, "The company could not be updated."));
					} else {
						return callback(null);
					}
				});
			} else {
				//Could not connect to database.
				return callback(Util.wrappedError(500, dbError));
			}
		});
	},
	createWorkspace: function (companyId, payload, callback) {
		//Check if the company id is a valid objectid
		if (!ObjectId.isValid(companyId)) {
			return callback(Util.wrappedError(400, "Invalid company id"), null);
		}
		//Check if display name is exist
		if (!payload || !payload.displayName) {
			return callback(Util.wrappedError(400, ValidationRules.workspace), null);
		}
		//Open DB connection
		dbContext.open(function (dbError, dbConnection) {
			if (dbConnection) {
				var workspaceObject = {
					_id: new ObjectId(),
					displayName: payload.displayName,
					name: payload.displayName.toLowerCase(),
					users: []
				};
				//select companies collection
				var companiesCollection = dbConnection.collection("companies");
				//Check if the object is unique in its context.
				companiesCollection.updateOne({
					_id: new ObjectId(companyId),
					"workspaces.name": {
						$ne: workspaceObject.name
					}
				}, {
					$push: {
						workspaces: workspaceObject
					}
				}, function (err, updateRes) {
					dbConnection.close();
					if (err) {
						//If the error is a validation error, return the error of validation, if not, return the error.
						return callback(Util.wrappedError(400, (err.code == VALIDATION_ERROR_CODE) ? ValidationRules.workspace : err), null);
					} else if (updateRes.matchedCount == 0) {
						return callback(Util.wrappedError(404, "The company not found or the workspace is already exist."), null);
					} else if (updateRes.modifiedCount == 0) {
						return callback(Util.wrappedError(500, "The workspace could not be created."), null);
					} else {
						return callback(null, workspaceObject);
					}
				});
			} else {
				//Could not connect to database.
				return callback(Util.wrappedError(500, dbError), null);
			}
		});
	},
	updateWorkspace: function (companyId, workspaceId, payload, callback) {
		//Check if the company id is a valid objectid
		if (!ObjectId.isValid(companyId)) {
			return callback(Util.wrappedError(400, "Invalid company id"));

		}
		//Check if the workspace id is a valid objectid
		if (!ObjectId.isValid(workspaceId)) {
			return callback(Util.wrappedError(400, "Invalid workspace id"));

		}
		//Check if display name is exist
		if (!payload || !payload.displayName) {
			return callback(Util.wrappedError(400, ValidationRules.workspace));

		}
		//Open DB connection
		dbContext.open(function (dbError, dbConnection) {
			if (dbConnection) {
				var workspaceUpdateObject = {
					displayName: payload.displayName,
					name: payload.displayName.toLowerCase()
				};
				//select companies collection
				var companiesCollection = dbConnection.collection("companies");
				//Check if the object is unique in its context.
				companiesCollection.updateOne({
					_id: new ObjectId(companyId),
					"workspaces.name": {
						$ne: workspaceUpdateObject.name
					},
					"workspaces._id": new ObjectId(workspaceId)
				}, {
					$set: {
						"workspaces.$.displayName": workspaceUpdateObject.displayName,
						"workspaces.$.name": workspaceUpdateObject.name
					}
				}, function (err, updateRes) {
					dbConnection.close();
					if (err) {
						//If the error is a validation error, return the error of validation, if not, return the error.
						return callback(Util.wrappedError(400, (err.code == VALIDATION_ERROR_CODE) ? ValidationRules.workspace : err));
					} else if (updateRes.matchedCount == 0) {
						return callback(Util.wrappedError(404, "The workspace not found or the workspace is already exist."));
					} else if (updateRes.modifiedCount == 0) {
						return callback(Util.wrappedError(500, "The workspace could not be updated."));
					} else {
						return callback(null);
					}
				});
			} else {
				//Could not connect to database.
				return callback(Util.wrappedError(500, dbError));
			}
		});
	},
	addUser: function (companyId, workspaceId, payload, callback) {
		//Check if the company id is a valid objectid
		if (!ObjectId.isValid(companyId)) {
			return callback(Util.wrappedError(400, "Invalid company id"));

		}
		//Check if the workspace id is a valid objectid
		if (!ObjectId.isValid(workspaceId)) {
			return callback(Util.wrappedError(400, "Invalid workspace id"));

		}
		//Check if email and role are exist
		if (!payload || !payload.email || !payload.role) {
			return callback(Util.wrappedError(400, ValidationRules.user));

		}
		//Open DB connection
		dbContext.open(function (dbError, dbConnection) {
			if (dbConnection) {
				var userObject = {
					email: payload.email,
					role: payload.role
				};
				//select companies collection
				var companiesCollection = dbConnection.collection("companies");
				companiesCollection.update({
					_id: new ObjectId(companyId),
					"workspaces": {
						//Check if the object is unique in its context.
						$elemMatch: {
							"_id": new ObjectId(workspaceId),
							"users.email": {
								$ne: userObject.email
							}
						}
					}
				}, {
					$push: {
						"workspaces.$.users": userObject
					}
				}, function (err, updateRes) {
					dbConnection.close();
					if (err) {
						//If the error is a validation error, return the error of validation, if not, return the error.
						return callback(Util.wrappedError(400, (err.code == VALIDATION_ERROR_CODE) ? ValidationRules.user : err));
					} else if (updateRes.result.n == 0) {
						return callback(Util.wrappedError(404, "The workspace not found or the user is already exist."));
					} else if (updateRes.result.nModified == 0) {
						return callback(Util.wrappedError(500, "The user could not be added."));
					} else {
						return callback(null);
					}
				});
			} else {
				//Could not connect to database.
				return callback(Util.wrappedError(500, dbError));
			}
		});
	},
	removeUser: function (companyId, workspaceId, payload, callback) {
		//Check if the company id is a valid objectid
		if (!ObjectId.isValid(companyId)) {
			return callback(Util.wrappedError(400, "Invalid company id"));

		}
		//Check if the workspace id is a valid objectid
		if (!ObjectId.isValid(workspaceId)) {
			return callback(Util.wrappedError(400, "Invalid workspace id"));

		}
		//Check if email and role are exist
		if (!payload || !payload.email || !payload.role) {
			return callback(Util.wrappedError(400, ValidationRules.user));

		}
		//Open DB connection
		dbContext.open(function (dbError, dbConnection) {
			if (dbConnection) {
				var userRemoveObject = {
					email: payload.email
				};
				//select companies collection
				var companiesCollection = dbConnection.collection("companies");
				companiesCollection.updateOne({
					_id: new ObjectId(companyId),
					"workspaces._id": new ObjectId(workspaceId)
				}, {
					$pull: {
						"workspaces.$.users": userRemoveObject
					}
				}, function (err, updateRes) {
					dbConnection.close();

					if (err) {
						//If the error is a validation error, return the error of validation, if not, return the error.
						return callback(Util.wrappedError(400, (err.code == VALIDATION_ERROR_CODE) ? ValidationRules.user : err));
					} else if (updateRes.matchedCount == 0) {
						return callback(Util.wrappedError(404, "The user not found or the user is already removed."));
					} else if (updateRes.modifiedCount == 0) {
						return callback(Util.wrappedError(500, "The user could not be removed."));
					} else {
						return callback(null);
					}
				});
			} else {
				//Could not connect to database.
				return callback(Util.wrappedError(500, dbError));
			}
		});

	},
	removeCompany: function (companyIds, callback) {
		//Open DB connection
		dbContext.open(function (dbError, dbConnection) {
			if (dbConnection) {
				var removeQuery = {
					_id: {
						$in: companyIds
					}
				};
				//select companies collection
				var companiesCollection = dbConnection.collection("companies");
				companiesCollection.remove(removeQuery, function (err, removeRes) {
					dbConnection.close();
					if (err) {
						return callback(Util.wrappedError(400, err), null);
					} else {
						return callback(null, removeRes.result);
					}
				});
			} else {
				//Could not connect to database.
				return callback(Util.wrappedError(500, dbError), null);
			}
		});
	}
};
module.exports = companiesService;