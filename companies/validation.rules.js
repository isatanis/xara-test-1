//validation rules are derivated from mongo json schema. 
var rules = {
	company: {
		displayName: {
			type: "string",
			minLength: 1,
			description: "must be a string, required and unique"
		}
	},
	workspace: {
		displayName: {
			type: "string",
			minLength: 1,
			description: "must be a string, required and unique"
		}
	},
	user: {
		role: {
			enum: ["basic", "admin"],
			description: "can only be one of the enum values"
		},
		email: {
			type: "string",
			// eslint-disable-next-line no-useless-escape
			pattern: "^.+\@.+$",
			description: "must be a valid email address and unique"
		}
	}
};
module.exports = rules;